//Xuanru Yang, 1942014
public class Vector3d{
    public static void main(String[] args) {
    }
    private double x;

    private double y;

    private double z;

    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public double magnitude() {
        double result = Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
        return result;
    }

    public double dotProduct(Vector3d a) {
        double result = this.x * a.x + this.y * a.y + this.z * a.z;
        return result;
    }

    public Vector3d add(Vector3d a) {
        Vector3d b = new Vector3d(0, 0, 0);
        b.x = this.x + a.x;
        b.y = this.y + a.y;
        b.z = this.z + a.z;
        return b;
    }
}
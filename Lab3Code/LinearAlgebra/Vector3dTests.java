//Xuanru Yang. 1942014
import static org.junit.jupiter.api.Assertions.*;
import java.util.Vector;
import org.junit.Test;
public class Vector3dTests {

    @Test
    public void test() {
        Vector3d vector = new Vector3d(1, 3, 4);
        assertEquals(1, vector.getX());
    }

    @Test
    public void test2() {
        Vector3d vector = new Vector3d(1, 3, 4);
        assertEquals(3, vector.getY());
    }

    @Test
    public void test3() {
        Vector3d vector = new Vector3d(1, 3, 4);
        assertEquals(4, vector.getZ());
    }

    @Test
    public void test4() {
        Vector3d vector = new Vector3d(2, 3, 6);
        assertEquals(7, vector.magnitude());
    }

    @Test
    public void test5() {
        Vector3d vector[] = new Vector3d[2];
        vector[0] = new Vector3d(2 ,2, 1);
        vector[1] = new Vector3d(1 ,3, 2);
        double test = vector[0].dotProduct(vector[1]);
        assertEquals(10, test);
    }

    @Test
    public void test6() {
        Vector3d vector[] = new Vector3d[2];
        vector[0] = new Vector3d(2 ,2, 1);
        vector[1] = new Vector3d(1 ,3, 2);
        Vector3d vectorTest = vector[0].add(vector[1]);
        assertEquals(3, vectorTest.getX());
        assertEquals(5, vectorTest.getY());
        assertEquals(3, vectorTest.getZ());
    }
}